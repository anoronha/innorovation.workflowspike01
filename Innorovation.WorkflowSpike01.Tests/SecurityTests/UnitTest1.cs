﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Security.Permissions;
using System.Collections.Generic;
using System.Linq;

namespace Innorovation.WorkflowSpike01.Tests.SecurityTests
{
    [TestClass]
    public class Security_Tests
    {
        [TestMethod]
        public void SecurityEvaluation()
        {
            //var fileIOPermission = new FileIOPermission(
        }
    }

    public class SecurityManager
    {
        static SecurityManager _instance;
        public static SecurityManager Instance
        {
            get
            {
                if (_instance == null) 
                    _instance = new SecurityManager();
                return _instance;
            }
        }


    }

    public class PermissionEntry
    {
        public string ResourceId { get; set; }
        public string AccessId { get; set; }
    }

    public class GrantEntry
    {
        public PermissionEntry Permission { get; set; }
        public string RoleId { get; set; }
    }

    public class EmployeeService
    {
        public Employee ChangeEmployee(int id, string name)
        {
            var employee = employees.Where(x => x.Id == id).FirstOrDefault();
            if (employee != null)
            {
                employee.Name = name;
            }
            else
            {
                throw new ApplicationException("Employee not found");
            }
            return employee;
        }

        public List<Employee> employees = new List<Employee>
        {
            new Employee{ Id=1, Name = "Arun Noronha"}
        };
    }

    public class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    /*
     access to an aspect of a resource has a permission 
     editing a user required a permission
     viewing a user requires a permission
     
     A permission set can also roll up multiple permissions 
           
     A permission is granted to a role
     
     group administrators at a group have access to all items under the group umbrella
     
     */
}
