﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Innorovation.WorkflowSpike01.Console;
using System.Collections.Generic;
using System.Linq;

namespace Innorovation.WorkflowSpike01.Tests
{
    [TestClass]
    public class TokenReaderTests
    {
        [TestMethod]
        public void TokenReader_ReturnString()
        {
            var text = "return \"one\";";
            ScriptTokenReader reader = new ScriptTokenReader();
            IEnumerable<Token> tokens  = reader.Parse(text);
            var result = tokens.ToList();
            Assert.AreEqual(3, result.Count());
            Assert.AreEqual(TokenType.Keyword, result[0].Type);
            Assert.AreEqual("return", result[0].Text);
            Assert.AreEqual(TokenType.String, result[1].Type);
            Assert.AreEqual("one", result[1].Text);
            Assert.AreEqual(TokenType.Semicolon, result[2].Type);
            Assert.AreEqual(";", result[2].Text);
        }
    }

    [TestClass]
    public class WorkflowEngineTests
    {
        [TestMethod]
        public void WorkflowEngine_SimpleTest()
        {
            WorkflowBuilder.Current
                .For("BankAccount")
                .HasStates
                (
                    "Provisioning",
                    "Active",
                    "Closed",
                    "Overdrawn"
                )
                .WithInitialState("Provisioning")
                //.WithActions
                //(
                //    "Create",   
                //    "Transfer",
                //    "Withdraw",
                //    "Close"
                //)
                .WithTransitionMapEntry
                (
                    fromStateName: "Provisioning",
                    actionName: "Create",
                    actionValidation: "Null",
                    actionResult: "CreationSuccessful",
                    actionScript: "",
                    toStateName: "Active"
                );
        }
    }
}



/*
Workflow 

Action sent over with Id and body

StateResolver gets the state
CommandValidation validates the command

Runs the command - script

The script at the end invokes a transition

- when a transition occurs and event is registered
- if a transition does not occur it is same


*/