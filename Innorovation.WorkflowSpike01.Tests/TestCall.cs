﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Innorovation.WorkflowSpike01.Console;
using System.Collections.Generic;

namespace Innorovation.WorkflowSpike01.Tests
{
    [TestClass]
    public class TestCall
    {
        [TestMethod]
        public void RunningAnAddOperationExecutesAddition()
        {
            IMethodCall methodCall = new ClrInstanceMethodCall("add","framework","Innorovation.WorkflowSpike01.Console.MathFunctions","Add");
            var scope = new ExecutionScope();
            scope.SetVariableValue("num1", 2);
            scope.SetVariableValue("num2", 1);
            var result = methodCall.Execute(scope);
            Assert.AreEqual(3,result);
        }

        [TestMethod]
        public void TestMethod()
        {
            //IMethodCall methodCall= new ScriptMethodCall(
        }


    }

    [TestClass]
    public class ConversionUtilTests
    {
        [TestMethod]
        public void Convert_Long_To_Int()
        {
            var output = ConversionUtil.Convert<long, int>(1000L);
            Assert.IsInstanceOfType(output, typeof(int));
            Assert.AreEqual(1000, output);
        }

        [TestMethod]
        public void Convert_Int_To_Long()
        {
            var output = ConversionUtil.Convert<int, long>(1000);
            Assert.IsInstanceOfType(output, typeof(long));
            Assert.AreEqual(1000L, output);
        }

        [TestMethod]
        public void Convert_Int_To_Float()
        {
            var output = ConversionUtil.Convert<int, float>(1000);
            Assert.IsInstanceOfType(output, typeof(float));
            Assert.AreEqual(1000.0f, output);
        }

        [TestMethod]
        public void Convert_Float_To_Int()
        {
            var output = ConversionUtil.Convert<float, int>(1000.234f);
            Assert.IsInstanceOfType(output, typeof(int));
            Assert.AreEqual(1000, output);
        }

        [TestMethod]
        public void Convert_Double_To_Float()
        {
            var output = ConversionUtil.Convert<double, float>(1000.234d);
            Assert.IsInstanceOfType(output, typeof(float));
            Assert.AreEqual(1000.234f, output);
        }

        [TestMethod]
        public void Convert_Float_To_Double()
        {
            var output = ConversionUtil.Convert<float, double>(1000.234f);
            Assert.IsInstanceOfType(output, typeof(double));
            Assert.IsTrue((1000.234d-output)<0.00001);
        }

        [TestMethod]
        public void Convert_Double_To_Decimal()
        {
            var output = ConversionUtil.Convert<double, decimal>(1000.234d);
            Assert.IsInstanceOfType(output, typeof(decimal));
            Assert.AreEqual(1000.234m, output);
        }

        [TestMethod]
        public void Convert_Decimal_To_Double()
        {
            var output = ConversionUtil.Convert<decimal, double>(1000.234m);
            Assert.IsInstanceOfType(output, typeof(double));
            Assert.IsTrue((1000.234d - output) < 0.00001);
        }

        [TestMethod]
        public void Convert_Float_To_Decimal()
        {
            var output = ConversionUtil.Convert<float, decimal>(1000.234f);
            Assert.IsInstanceOfType(output, typeof(decimal));
            Assert.AreEqual(1000.234m, output);
        }

        [TestMethod]
        public void Convert_Decimal_To_Float()
        {
            var output = ConversionUtil.Convert<decimal, float>(1000.234m);
            Assert.IsInstanceOfType(output, typeof(float));
            Assert.IsTrue((1000.234f - output) < 0.00001);
        }

        [TestMethod]
        public void Convert_String_To_Int()
        {
            var output = ConversionUtil.Convert<string, int>("123");
            Assert.IsInstanceOfType(output, typeof(int));
            Assert.AreEqual(123, output);
        }

        [TestMethod]
        public void Convert_String_To_Decimal()
        {
            var output = ConversionUtil.Convert<string, decimal>("123.234");
            Assert.IsInstanceOfType(output, typeof(decimal));
            Assert.AreEqual(123.234m, output);
        }
    }
}
