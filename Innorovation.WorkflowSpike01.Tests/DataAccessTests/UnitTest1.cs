﻿//#define INCLUDETESTS
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Innorovation.WorkflowSpike01.DataAccess;
using Innorovation.WorkflowSpike01.Console;

namespace Innorovation.WorkflowSpike01.Tests.DataAccessTests
{
    [TestClass]
    public class DataAccessTests
    {
#if INCLUDETESTS
        [TestMethod]
#endif
        public void SaveEntity()
        {
            var user = new User { FirstName = "Arun", LastName = "Noronha" };
            RavenDataAccess dataAccess = new RavenDataAccess("TestDb");
            dataAccess.Save<User>(user);
        }

#if INCLUDETESTS
        [TestMethod]
#endif
        public void GetEntity()
        {
            RavenDataAccess dataAccess = new RavenDataAccess("TestDb");
            RavenQuery<User> userQuery = new RavenQuery<User>();
            //userQuery.AddCriterion(x => x.FirstName == "DArun");
            userQuery.AddSort<string>(x => x.LastName, descending:false);
            var users = dataAccess.Query(userQuery);
        }


#if INCLUDETESTS
        [TestMethod]
#endif
        public void GetPagedEntity()
        {
            RavenDataAccess dataAccess = new RavenDataAccess("TestDb");
            RavenQuery<User> userQuery = new RavenQuery<User>();
            //userQuery.AddSearchCriterion(x => x.FirstName, "Arun");
            userQuery.AddSort<string>(x => x.LastName, descending: false);
            userQuery.PageIndex = 0;
            userQuery.PageSize = 1;
            var users = dataAccess.Query(userQuery);
        }


#if INCLUDETESTS
        [TestMethod]
#endif
        public void WorkflowService_SaveAllClrInstanceScripts()
        {
            IWorkflowRepository repo = new DefaultWorkflowRepository();
            WorkflowService service = new WorkflowService(repo);
            service.SaveAllClrInstanceScripts();
        }


#if INCLUDETESTS
        [TestMethod]
#endif
        public void WorkflowService_GetScripts()
        {
            IWorkflowRepository repo = new DefaultWorkflowRepository();
            WorkflowService service = new WorkflowService(repo);
            service.LoadMethodCallCatalog();
        }
    }

    public class User
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
