﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Innorovation.WorkflowSpike01.Console;

namespace Innorovation.WorkflowSpike01.Tests
{
    [TestClass]
    public class ScriptParserTests
    {
        [TestMethod]
        public void ReturnStatement_String_Execution()
        {
            var script = "return \"one\";";
            ScriptParser parser = new ScriptParser();
            IExecutionUnit unit = parser.Parse(script);
            var result = unit.Execute(null);
            Assert.IsInstanceOfType(unit, typeof(ScriptBody));
            Assert.AreEqual("one", result);
        }

        [TestMethod]
        public void ReturnStatement_Int_Execution()
        {
            var script = "return 1;";
            ScriptParser parser = new ScriptParser();
            IExecutionUnit unit = parser.Parse(script);
            var result = unit.Execute(null);
            Assert.IsInstanceOfType(unit, typeof(ScriptBody));
            Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void ReturnStatement_Int_Add_Execution()
        {
            var script = "return 1+1;";
            ScriptParser parser = new ScriptParser();
            IExecutionUnit unit = parser.Parse(script);
            var result = unit.Execute(null);
            Assert.IsInstanceOfType(unit, typeof(ScriptBody));
            Assert.AreEqual(2, result);
        }

        [TestMethod]
        public void ReturnStatement_Int_Subtract_Execution()
        {
            var script = "return 3 - 2 ;";
            ScriptParser parser = new ScriptParser();
            IExecutionUnit unit = parser.Parse(script);
            var result = unit.Execute(null);
            Assert.IsInstanceOfType(unit, typeof(ScriptBody));
            Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void ReturnStatement_Int_Multiply_Execution()
        {
            var script = "return 3 * 5 ;";
            ScriptParser parser = new ScriptParser();
            IExecutionUnit unit = parser.Parse(script);
            var result = unit.Execute(null);
            Assert.IsInstanceOfType(unit, typeof(ScriptBody));
            Assert.AreEqual(15, result);
        }

        [TestMethod]
        public void ReturnStatement_Int_Divide_Execution()
        {
            var script = "return 8 / 4 ;";
            ScriptParser parser = new ScriptParser();
            IExecutionUnit unit = parser.Parse(script);
            var result = unit.Execute(null);
            Assert.IsInstanceOfType(unit, typeof(ScriptBody));
            Assert.AreEqual(2, result);
        }

        [Ignore]
        public void ReturnStatement_Int_Divide_By_Zero_Error_Execution()
        {
            //should have an error variable with error message
            var script = "return 8 / 0 ;";
            ScriptParser parser = new ScriptParser();
            IExecutionUnit unit = parser.Parse(script);
            var result = unit.Execute(null);
            Assert.IsInstanceOfType(unit, typeof(ScriptBody));
            Assert.AreEqual(2, result);
        }


        [TestMethod]
        public void MethodDelegation_Min()
        {
            var scriptBody = "return framework.min(val1,val2);";
            IMethodCatalog catalog = new MockMethodCatalog();
            catalog.AddMethod(
    new ClrInstanceMethodCall("min", "framework", "Innorovation.WorkflowSpike01.Console.MathFunctions", "Min"));

            catalog.AddScriptMethod("framework.math", "min",scriptBody,"val1","val2");
            
            var script = "return framework.math.min(17,19);";
            ScriptParser parser = new ScriptParser(catalog);
            IExecutionUnit unit = parser.Parse(script);
            var result = unit.Execute(null);
            Assert.IsInstanceOfType(unit, typeof(ScriptBody));
            Assert.AreEqual(17, result);

        }

        [TestMethod]
        public void MethodDelegation_Add()
        {
            var scriptBody = "return framework.add(val1,val2);";
            IMethodCatalog catalog = new MockMethodCatalog();
    //        catalog.AddMethod(
    //new ClrInstanceMethodCall("add", "framework", "Innorovation.WorkflowSpike01.Console.MathFunctions", "Add"));

            catalog.AddScriptMethod("framework.math", "add", scriptBody, "val1", "val2");

            var script = "return framework.math.add(17,19);";
            ScriptParser parser = new ScriptParser(catalog);
            IExecutionUnit unit = parser.Parse(script);
            var result = unit.Execute(null);
            Assert.IsInstanceOfType(unit, typeof(ScriptBody));
            Assert.AreEqual(36, result);

        }

        [TestMethod]
        public void MethodDirect_ClrInstance_Subtract()
        {
            //var scriptBody = "return framework.add(val1,val2);";
            IMethodCatalog catalog = new MockMethodCatalog();
            catalog.AddMethod(
    new ClrInstanceMethodCall("subtract", "framework", "Innorovation.WorkflowSpike01.Console.MathFunctions", "Subtract"));

            //catalog.AddScriptMethod("framework.math", "add", scriptBody, "val1", "val2");

            var script = "return framework.subtract(17,19);";
            ScriptParser parser = new ScriptParser(catalog);
            IExecutionUnit unit = parser.Parse(script);
            var result = unit.Execute(null);
            Assert.IsInstanceOfType(unit, typeof(ScriptBody));
            Assert.AreEqual(-2, result);

        }
    }
}
