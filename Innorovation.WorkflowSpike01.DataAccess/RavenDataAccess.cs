﻿using Raven.Abstractions.Data;
using Raven.Client;
using Raven.Client.Document;
using Raven.Client.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Innorovation.WorkflowSpike01.DataAccess
{
    public class RavenDataAccess
    {
        public RavenDataAccess(string database)
        {
            IDocumentStore store = new DocumentStore { Url = "http://localhost:8080" };
            _database = database;
            _store = store;
            InitializeStore();
        }

        private void InitializeStore()
        {
            _store.Initialize();
            _store.Conventions.FindTypeTagName = FindTagNameForType;
        }

        IDocumentStore _store;
        string _database;
        private Dictionary<Type, string> _collectionOverrides = new Dictionary<Type, string>();
        private Dictionary<Type, string> _idOverrides = new Dictionary<Type, string>();


        public void AddRavenCollectionOverrideForType(Type type, string collectionName)
        {
            _collectionOverrides.Add(type, collectionName);
        }

        private string FindTagNameForType(Type type)
        {
            if (_collectionOverrides.ContainsKey(type))
            {
                return _collectionOverrides[type];
            }
            return type.Name;
        }

        public IDocumentSession NewSession()
        {
            OpenSessionOptions options = new OpenSessionOptions();
            options.Database = _database;
            return _store.OpenSession(options);
        }

        public List<T> GetByIds<T>(params string[] ids)
        {
            using (var session = NewSession())
            {
                var results = session.Load<T>(ids);
                return results.ToList();
            }
        }

        public QueryResult<T> Query<T>(RavenQuery<T> query)
        {
            using (var session = NewSession())
            {
                IQueryable<T> executableQuery = null;
                RavenQueryStatistics stats=null;
                var querySession = session.Query<T>().Statistics(out stats);
                    ;
                if (query.Criteria != null)
                {
                    foreach (var criterion in query.Criteria)
                    {
                        querySession = querySession.Where(criterion);
                    }
                }
                if (query.SearchCriteria != null)
                {
                    foreach (var criterion in query.SearchCriteria)
                    {
                        querySession = querySession.Search(criterion.Item1, criterion.Item2);
                    }
                }
                if (query.Sorts != null)
                {
                    int index = 0;
                    IOrderedQueryable<T> orderedQuerySession = querySession;
                    foreach (var sort in query.Sorts)
                    {
                        orderedQuerySession = Sort<T, object>(orderedQuerySession, sort.Item1, index, sort.Item2);
                    }
                    executableQuery = orderedQuerySession;
                }
                else
                {
                    executableQuery = querySession;
                }
                int pageSize = query.PageSize;
                int pageIndex = query.PageIndex;
                if (query.PageSize <= 0 || query.PageSize>5000)
                {
                    pageSize = 100;
                }
                
                var results = executableQuery
                    .Skip(pageIndex * pageSize)
                    .Take(pageSize)
                    .ToList();

                QueryResult<T> queryResults = new QueryResult<T>();
                queryResults.PageIndex = pageIndex;
                queryResults.PageSize = pageSize;
                queryResults.TotalRows = stats.TotalResults;
                queryResults.IsStale = stats.IsStale;
                queryResults.IndexName = stats.IndexName;
                queryResults.IndexEtag = stats.IndexEtag;
                queryResults.IndexTimestamp = stats.IndexTimestamp;
                queryResults.Results = results;
                return queryResults;
            } 
        }

        public IOrderedQueryable<T> Sort<T, TKey>(IOrderedQueryable<T> query, Expression<Func<T, TKey>> sortKey, int index, bool descending)
        {
            if (index == 0)
            {
                var ravenQuery = (IRavenQueryable<T>)query;
                if (descending)
                {
                    return ravenQuery.OrderByDescending<T, TKey>(sortKey);
                }
                else
                {
                    return ravenQuery.OrderBy<T, TKey>(sortKey);
                }
            }
            else
            {
                if (descending)
                {
                    return query.ThenByDescending<T, TKey>(sortKey);
                }
                else
                {
                    return query.ThenBy<T, TKey>(sortKey);
                }
            }
        }

        public void Save<T>(T item)
        {
            using (var session = NewSession())
            {
                if (!EnsureId(item)) return;
                string id = GetIdFor(item);
                if (id == null) return;
                if (!session.Advanced.IsLoaded(id))
                {
                    session.Store(item, id);
                }
                session.SaveChanges();
            }
        }

        private string GetIdFor(object item)
        {
            if(item==null) return null;
            var type = item.GetType();
            var idPropertyName = GetIdPropertyName(type);
            var propInfo = type.GetProperty(idPropertyName);
            var idValue = propInfo.GetValue(item);
            if (idValue == null) return Guid.NewGuid().ToString();
            return idValue.ToString();
        }

        private string GetIdPropertyName(Type type)
        {
            if (_idOverrides.ContainsKey(type))
            {
                return _idOverrides[type];
            }
            return "Id";
        }

        private bool EnsureId(object item)
        {
            if (item == null) return false;
            var type = item.GetType();
            var idPropertyName = GetIdPropertyName(type);
            var propInfo = type.GetProperty(idPropertyName);
            if (propInfo == null) 
                throw new ApplicationException("Id property does not exist");
            var idValue = propInfo.GetValue(item);
            if (idValue == null || idValue == Guid.Empty.ToString())
            {
                idValue = Guid.NewGuid().ToString();
                propInfo.SetValue(item, idValue);
            }
            return true;
        }

        public void AddRavenIdOverrideForType(Type type, string idFieldName)
        {
            _idOverrides.Add(type, idFieldName);
        }
    }

    public class QueryResult<T>
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public List<T> Results { get; set; }
        public int TotalRows { get; set; }
        public bool IsStale { get; set; }
        public string IndexName { get; set; }
        public Etag IndexEtag { get; set; }
        public DateTime IndexTimestamp { get; set; }
    }

    public class RavenQuery<T>
    {
        public RavenQuery()
        {
            Criteria = new List<Expression<Func<T, bool>>>();
            SearchCriteria = new List<Tuple<Expression<Func<T, object>>, string>>();
            Sorts = new List<Tuple<Expression<Func<T, object>>, bool, Type>>();
        }

        public List<Expression<Func<T, bool>>> Criteria { get; private set; }
        public List<Tuple<Expression<Func<T, object>>,string>> SearchCriteria { get; private set; }
        public List<Tuple<Expression<Func<T, object>>, bool, Type>> Sorts { get; private set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public bool ReadTillEnd { get; set; }

        public void AddCriterion(Expression<Func<T, bool>> criterion)
        {
            Criteria.Add(criterion);
        }

        public void AddSearchCriterion(Expression<Func<T, object>> searchCriterion, string value)
        {
            SearchCriteria.Add(new Tuple<Expression<Func<T,object>>,string>(searchCriterion,value));
        }

        public void AddSort<TKey>(Expression<Func<T, TKey>> sort, bool descending)
        {
            var normalizedSort = Convert(sort);
            Sorts.Add(new Tuple<Expression<Func<T, object>>, bool, Type>(normalizedSort, descending, typeof(TKey)));
        }

        public Expression<Func<T, object>> Convert<TKey>(Expression<Func<T, TKey>> expression)
        {
            var propertyName = ((MemberExpression)expression.Body).Member.Name;
            var convertedExpression = LambdaExpression.Lambda<Func<T,object>>(LambdaExpression.Property(Expression.Parameter(typeof(T),"x"), propertyName),
                Expression.Parameter(typeof(T), "x"));
            return convertedExpression;
        }
    }
}
