﻿using Innorovation.ControlCode.Razor;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Innorovation.SiteSkeleton.Controllers
{
    public class HomeController : Controller
    {
        static ViewGenerator generator = new ViewGenerator();
        static HomeController()
        {
            generator.AddControls(new Dictionary<string, string>
            {
                {"span","<span>@Model.Name</span>"}
            });
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ControlDefinition()
        {
            return View();
        }

        public ActionResult ViewRendering()
        {
            return View();
        }

        public ActionResult RenderMarkupHtml(MarkupData data)
        {
            var markup = data.Markup;
            generator.AddControl(data.Name, data.Markup);
            var json = JObject.Parse(data.Model??"{}");
            var result = generator.RenderView(data.Name, json);
            return Content(result);
        }
    }

    public class MarkupData
    {
        public string Name { get; set; }
        public string Markup { get; set; }
        public string Model { get; set; }
    }
}
