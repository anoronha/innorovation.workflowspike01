﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innorovation.WorkflowSpike01.Console
{
    public class InterrogatorContext
    {
        public void WriteLine(string format, params string[] values)
        {
            System.Console.WriteLine(format, values);
        }

        public string ReadLine(string format, params string[] values)
        {
            WriteLine(format, values);
            return ReadLine();
        }

        public string ReadTillEmptyLine(int lineCount, string format, params string[] values)
        {
            WriteLine(format, values);
            return ReadTillEmptyLine(lineCount);
        }

        public string ReadLine()
        {
            return System.Console.ReadLine();
        }

        public string ReadTillEmptyLine(int lineCount = 1)
        {
            if (lineCount < 1) throw new ApplicationException("Line count cannot be less than 1");
            int currentEmptyLineCount = 0;
            string output = "";
            do
            {
                var currentLine = System.Console.ReadLine();
                if (currentLine.Trim() == string.Empty)
                {
                    currentEmptyLineCount++;
                }
                else
                {
                    currentEmptyLineCount = 0;
                    output += currentLine;
                }
            } while (currentEmptyLineCount < lineCount);
            return output;
        }

        internal static InterrogatorContext Create()
        {
            return new InterrogatorContext();
        }
    }

    public class Interrogator
    {
        List<ChoicePrompt> prompts = new List<ChoicePrompt>();
        List<ChoiceStep> steps = new List<ChoiceStep>();

        public Interrogator()
        {
            ConfigureExit();
        }

        public Interrogator ConfigureStep(string name, Action<InterrogatorContext> action)
        {
            return ConfigureStep(new ChoiceStep { Name = name, ActionToPerform = action });            
        }

        public Interrogator ConfigureStep(ChoiceStep step)
        {
            steps.Add(step);
            return this;
        }

        public Interrogator ConfigurePrompt(string name, string prompt = "", string stepName = null, string defaultContinuation = "")
        {
            ChoicePrompt choicePrompt = new ChoicePrompt { Name = name, Prompt = prompt, Interrogator = this, StepName = stepName, DefaultContinuation = defaultContinuation };
            prompts.Add(choicePrompt);
            return this;
        }

        private ChoiceStep LookupStep(string stepName)
        {
            return steps.Where(x => x.Name == stepName).FirstOrDefault();
        }

        public Interrogator ConfigureOption(string optionPrompt, string value, string stepName, string continuation)
        {
            var lastPrompt = prompts.LastOrDefault();
            if (lastPrompt != null)
            {
                lastPrompt.Options.Add(new ChoicePromptOption
                {
                    Prompt = optionPrompt,
                    Value = value,
                    StepName = stepName,
                    DefaultContinuation = continuation
                });
            }
            return this;
        }

        private void ConfigureExit()
        {
            steps.Add(ChoiceStep.EXIT_STEP);
            ConfigurePrompt(ChoiceStep.GetExitStepName(), ChoiceStep.GetExitStepName(), ChoiceStep.GetExitStepName());
        }

        private void Exit(string parameter, InterrogatorContext context)
        {
            System.Console.WriteLine("Press any key to exit...");
            var key = System.Console.ReadKey();
            Environment.Exit(0);
        }

        public void Execute(string prompt)
        {
            var currentPrompt = GetPromptByName(prompt);

            do
            {
                var continuation = currentPrompt.Execute();
                currentPrompt = GetPromptByName(continuation);
            } while (currentPrompt != null);
            Exit("", InterrogatorContext.Create());
        }

        private ChoicePrompt GetPromptByName(string prompt)
        {
            return prompts.Where(x => x.Name == prompt).FirstOrDefault();
        }

        internal List<ChoiceStep> GetSteps()
        {
            return steps;
        }
    }

    public class ChoicePrompt
    {
        public ChoicePrompt()
        {
            Options = new List<ChoicePromptOption>();
        }
        public string Name { get; set; }
        public List<ChoicePromptOption> Options { get; set; }
        public Interrogator Interrogator { get; set; }
        public string StepName { get; set; }
        public string Prompt { get; set; }
        public string DefaultContinuation { get; set; }
        public List<ChoiceStep> Steps { get { return Interrogator.GetSteps(); } }

        public string Execute()
        {

            if (Options.Count > 0)
            {
                //Present options 
                var prompts = Options.Select(x => x.Prompt).ToList();
                prompts.Insert(0, Prompt);
                prompts = prompts.Where(x => !string.IsNullOrEmpty(x)).ToList();
                DisplayPrompt(prompts.ToArray());
                var response = System.Console.ReadLine();
                var optionSelected = Options.Where(x => x.Value == response).FirstOrDefault();
                if (optionSelected == null && ContainsSingleActionableEntry())
                    optionSelected = GetSingleActionableEntry();
                if (optionSelected != null)
                {
                    RunStep(optionSelected.StepName, InterrogatorContext.Create());
                    return optionSelected.DefaultContinuation;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(this.StepName))
                {
                    RunStep(this.StepName, InterrogatorContext.Create());
                }
                return DefaultContinuation;
            }
            return string.Empty;

        }

        private void RunStep(string stepName, InterrogatorContext context)
        {
            var step = Steps.Where(x => x.Name == stepName).FirstOrDefault();
            if (step == null) throw new ApplicationException(string.Format("step with name {0} was not found", stepName));
            step.ActionToPerform(context);
        }

        private ChoicePromptOption GetSingleActionableEntry()
        {
            return Options.Where(x => x.Prompt != ChoiceStep.GetExitStepName()).First();
        }

        private bool ContainsSingleActionableEntry()
        {
            return Options.Where(x => x.Prompt != ChoiceStep.GetExitStepName()).Count() == 1;
        }

        public static void DisplayPrompt(params string[] messages)
        {
            var color = System.Console.ForegroundColor;
            System.Console.ForegroundColor = ConsoleColor.Yellow;
            foreach (var message in messages)
            {
                System.Console.WriteLine(message);
            }
            System.Console.ForegroundColor = color;
        }
    }

    public class ChoicePromptOption
    {
        public string Prompt { get; set; }
        public string Value { get; set; }
        public string StepName { get; set; }
        public string DefaultContinuation { get; set; }
    }


    public class ChoiceStep
    {
        public string Name { get; set; }
        public Action<InterrogatorContext> ActionToPerform { get; set; }

        public static string GetExitStepName() { return EXIT_STEP.Name; }
        public static ChoiceStep EXIT_STEP = CreateExit();//factory.Value;

        private static Lazy<ChoiceStep> factory = new Lazy<ChoiceStep>(CreateExit);

        private static ChoiceStep CreateExit(){
            var exit = new ChoiceStep
            {
                Name = "Exit",
                ActionToPerform = (context) =>
                {
                    System.Console.WriteLine("Press any key to exit...");
                    var key = System.Console.ReadKey();
                    Environment.Exit(0);
                }
            };
            return exit;
        }
    }
}
