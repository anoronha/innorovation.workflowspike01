﻿using Innorovation.WorkflowSpike01.DataAccess;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Innorovation.WorkflowSpike01.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            Interrogator gator = SetupPrompts();
            gator.Execute("Prompt1");
        }

        private static Interrogator SetupPrompts()
        {
            Interrogator interrogator = new Interrogator();
            interrogator
                /*Adding steps that will contain actions*/
                .ConfigureStep("Step1", (context) => context.WriteLine("this is step 1"))
                .ConfigureStep("Step2", (context) => context.WriteLine("this is step 2"))
                .ConfigureStep("Step3", (context) => context.WriteLine("this is step 3"))
                .ConfigureStep("Create Script", CreateScript)
                /*Adding prompts to notify users and solicit actions*/
                .ConfigurePrompt("Prompt1", "Level 1")
                    .ConfigureOption("Thing one", "1", "Step1", "Prompt1")
                    .ConfigureOption("Thing two", "2", "Step2", "Prompt2")
                .ConfigurePrompt("Prompt2", "Level 2")
                    .ConfigureOption("Thing one", "1", "Step1", "Prompt1")
                    .ConfigureOption("Thing two", "2", "Step2", "Prompt2")
                    .ConfigureOption("Thing three", "3", "Step3", "Prompt3")
                .ConfigurePrompt("Prompt3", "Level 3")
                    .ConfigureOption("Thing one", "1", "Step1", "Prompt1")
                    .ConfigureOption("Thing two", "2", "Step2", "Prompt2")
                    .ConfigureOption("Thing three", "3", "Step3", "Prompt3")
                ;
            return interrogator;
        }

        private static void CreateScript(InterrogatorContext context)
        {
            context.WriteLine("Enter a name for the script");
            var scriptName = context.ReadLine();
            context.WriteLine("Enter a namespace for the script");
            var scriptNamespace  = context.ReadLine();
            context.WriteLine("Enter the parameter names of the script");
            var parameterNames = context.ReadTillEmptyLine();
            context.WriteLine("Enter the body of the script");
            var scriptBody = context.ReadTillEmptyLine();

            IWorkflowRepository repo = new DefaultWorkflowRepository();
            WorkflowService service = new WorkflowService(repo);
            service.SaveScript(scriptName, scriptNamespace, scriptBody, parameterNames);
        }
    }

    public class WorkflowService
    {
        public WorkflowService(IWorkflowRepository repo)
        {
            _repo = repo;
            InitializeService();
        }

        IWorkflowRepository _repo;
        IMethodCatalog _catalog;

        public void SaveScript(string scriptName, string scriptNamespace, string scriptBody, params string[] parameterNames)
        {
            var script = new ScriptMethodCall(scriptName, scriptNamespace, scriptBody, _catalog, parameterNames);
            _repo.SaveScript(script);
        }

        public void SaveAllClrInstanceScripts()
        {
            List<IMethodCall> calls = new List<IMethodCall>();
            var members = this.GetType().Assembly.GetTypes()
                .SelectMany(x=>x.GetMembers());

            foreach (var member in members)
            {
                var attributes = member.GetCustomAttributes<ClrInstanceMethodDeclarationAttribute>();
                if (!attributes.Any()) continue;
                foreach (var attribute in attributes)
                {
                    var clrInstanceScript = new ClrInstanceMethodCall(attribute.MethodName, attribute.MethodNamespace,
                         string.Format("{0}.{1}", member.DeclaringType.Namespace, member.DeclaringType.Name), member.Name);
                    calls.Add(clrInstanceScript);
                }
            }

            _repo.SaveScripts(calls);

        }

        private void InitializeService()
        {
            LoadMethodCallCatalog();
        }

        public void LoadMethodCallCatalog()
        {
            List<IMethodCall> methodCalls = _repo.GetScripts();
            IMethodCatalog catalog = new DefaultMethodCatalog();
            catalog.AddMethods(methodCalls);
            _catalog = catalog;
        }
    }

    public class ExecutionScope
    {
        public ExecutionScope()
        {
            Variables = new Dictionary<string, object>();
        }
        public Dictionary<string, object> Variables { get; private set; }

        public dynamic GetVariableValue(string _string)
        {
            if (!Variables.ContainsKey(_string)) return null; 
            return Variables[_string];
        }

        public void SetVariableValue(string _string, dynamic value)
        {
            if (!Variables.ContainsKey(_string)) 
                Variables.Add(_string,value);
            else 
                Variables[_string]=value;
        }
    }

    public interface IExecutionUnit
    {
        dynamic Execute(ExecutionScope context);
    }

    public abstract class ExecutionUnit:IExecutionUnit
    {
        public dynamic Execute(ExecutionScope context)
        {
            context = PrepareContext(context);
            return ExecuteImpl(context);
        }

        protected abstract dynamic ExecuteImpl(ExecutionScope context);

        protected virtual ExecutionScope PrepareContext(ExecutionScope context)
        {
            if (context == null) context = new ExecutionScope();
            return context;
        }
    }

    public class ScriptBlock : IExecutionUnit
    {
        public ScriptBlock(string fullName, ScriptBody scriptBody)
        {
            var indexToNamespaceSeparator = fullName.LastIndexOf('.');
            string methodNamespace = fullName.Substring(0, indexToNamespaceSeparator);
            string methodName = fullName.Substring(indexToNamespaceSeparator + 1, fullName.Length - (indexToNamespaceSeparator + 1));
            Name = methodName;
            Namespace = methodNamespace;
        }
        public string Name { get; set; }
        public string Namespace { get; set; }
        ScriptBody _body;

        public dynamic Execute(ExecutionScope context)
        {
            return _body.Execute(context);
        }
    }

    public class ScriptBody : IExecutionUnit
    {
        public ScriptBody(List<IStatement> statements)
        {
            _statements = statements;
        }

        List<IStatement> _statements;

        public dynamic Execute(ExecutionScope context)
        {
            dynamic returnValue = null;
            for (int i = 0; i < _statements.Count;i++ )
            {
                var statement = _statements[i];
                var result = statement.Execute(context);
                if (i == _statements.Count - 1 && statement is ReturnStatement)
                {
                    returnValue = result;
                }
            }
            return returnValue;
        }
    }

    public interface IStatement : IExecutionUnit
    {
    }

    public class VariableDeclarationStatement : ExecutionUnit, IStatement
    {
        public VariableDeclarationStatement(string variableName, IExpression value)
        {
            _variableName = variableName;
            _value = value;
        }

        string _variableName;
        IExpression _value;

        protected override dynamic ExecuteImpl(ExecutionScope context)
        {
            var variableValue = _value.Execute(context);
            context.SetVariableValue(_variableName, variableValue);
            return null;
        }
    }

    public class AssignmentStatement : ExecutionUnit, IStatement
    {
        public AssignmentStatement(string variableName, IExpression value)
        {
            _variableName = variableName;
            _value = value;
        }

        string _variableName;
        IExpression _value;

        protected override dynamic ExecuteImpl(ExecutionScope context)
        {
            var variableValue = _value.Execute(context);
            context.SetVariableValue(_variableName, variableValue);
            return null;
        }
    }

    public class ReturnStatement : ExecutionUnit,IStatement
    {
        public ReturnStatement(IExpression expression)
        {
            Children = new List<IExecutionUnit>();
            Children.Add(expression);
        }
        
        List<IExecutionUnit> Children { get; set; }
        protected override dynamic ExecuteImpl(ExecutionScope context)
        {
            IExpression expression = GetExpression();
            return expression.Execute(context);
        }

        IExpression GetExpression()
        {
            return Children.Cast<IExpression>().FirstOrDefault();
        }
    }

    public interface IExpression : IExecutionUnit
    {
    }

    public class StringExpression : IExpression
    {
        public StringExpression(string str)
        {
            _string = str;
        }
        string _string;
        public dynamic Execute(ExecutionScope context)
        {
            return _string;
        }
    }

    public class NumericExpression : IExpression
    {
        public NumericExpression(string str)
        {
            _string = str;
        }
        string _string;
        public dynamic Execute(ExecutionScope context)
        {
            if (IsFloating())
            {
                return Double.Parse(_string);
            }
            else
            {
                return Int64.Parse(_string);
            }
        }

        private bool IsFloating()
        {
            return _string.Contains('.');
        }
    }

    public class VariableExpression : IExpression
    {
        public VariableExpression(string str)
        {
            _string = str;
        }
        string _string;
        public dynamic Execute(ExecutionScope context)
        {
            if (context == null) throw new ApplicationException("scope null so variable cannot be evaluated");
            return context.GetVariableValue(_string);
        }
    }

    public abstract class BinaryExpression : ExecutionUnit, IExpression
    {
        public BinaryExpression(IExpression lhs, IExpression rhs)
        {
            _lhs = lhs;
            _rhs = rhs;
        }
        protected IExpression _lhs;
        protected IExpression _rhs;
    }

    public class AdditionExpression : BinaryExpression
    {
        public AdditionExpression(IExpression lhs, IExpression rhs) : base(lhs, rhs) { }

        protected override dynamic ExecuteImpl(ExecutionScope context)
        {
            dynamic lhsResult = _lhs.Execute(context);
            dynamic rhsResult = _rhs.Execute(context);
            return lhsResult + rhsResult;
        }
    }

    public class SubtractionExpression : BinaryExpression
    {
        public SubtractionExpression(IExpression lhs, IExpression rhs) : base(lhs, rhs) { }

        protected override dynamic ExecuteImpl(ExecutionScope context)
        {
            dynamic lhsResult = _lhs.Execute(context);
            dynamic rhsResult = _rhs.Execute(context);
            return lhsResult - rhsResult;
        }
    }

    public class MultiplicationExpression : BinaryExpression
    {
        public MultiplicationExpression(IExpression lhs, IExpression rhs) : base(lhs, rhs) { }

        protected override dynamic ExecuteImpl(ExecutionScope context)
        {
            dynamic lhsResult = _lhs.Execute(context);
            dynamic rhsResult = _rhs.Execute(context);
            return lhsResult * rhsResult;
        }
    }

    public class DivisionExpression : BinaryExpression
    {
        public DivisionExpression(IExpression lhs, IExpression rhs) : base(lhs, rhs) { }

        protected override dynamic ExecuteImpl(ExecutionScope context)
        {
            dynamic lhsResult = _lhs.Execute(context);
            dynamic rhsResult = _rhs.Execute(context);
            return lhsResult / rhsResult;
        }
    }

    public class ModuloExpression : BinaryExpression
    {
        public ModuloExpression(IExpression lhs, IExpression rhs) : base(lhs, rhs) { }

        protected override dynamic ExecuteImpl(ExecutionScope context)
        {
            dynamic lhsResult = _lhs.Execute(context);
            dynamic rhsResult = _rhs.Execute(context);
            return lhsResult % rhsResult;
        }
    }

    public class ConcatenationExpression : BinaryExpression
    {
        public ConcatenationExpression(IExpression lhs, IExpression rhs) : base(lhs, rhs) { }

        protected override dynamic ExecuteImpl(ExecutionScope context)
        {
            dynamic lhsResult = _lhs.Execute(context);
            dynamic rhsResult = _rhs.Execute(context);
            return string.Concat(lhsResult.ToString(), rhsResult.ToString());
        }
    }

    public class MethodCallExpression:ExecutionUnit,IExpression
    {
        public MethodCallExpression(IMethodCall call, params IExpression[] parameters)
        {
            _call = call;
            _parameters = parameters;
        }

        IExpression[] _parameters;
        private IMethodCall _call;
        protected override dynamic ExecuteImpl(ExecutionScope context)
        {
            dynamic[] callParams = _parameters.Select(x =>
                                                        {
                                                            return x.Execute(context);
                                                        }).ToArray();
            var parameters = _call.GetParameters();
            int index = 0;
            foreach (var parameter in parameters)
            {
                var parameterValue = callParams[index];
                context.SetVariableValue(parameter, parameterValue);
                index++;
            }
            var result = _call.Execute(context);
            return result;
        }
    }

    public interface IMethodCall : IExecutionUnit
    {
        string Namespace { get; set; }
        string Name { get; set; }
        string FullName { get; }
        string[] GetParameters();
    }
    
    public interface IMethodCatalog
    {
        IMethodCall GetMethod(string methodNamespace, string methodName);
        IMethodCall GetMethod(string fullMethodName);
        void AddScriptMethod(string methodNameSpace, string methodName, string methodBody, params string[] parameterNames);
        void AddMethod(ClrInstanceMethodCall call);

        void AddMethods(IEnumerable<IMethodCall> methodCalls);
    }

    public class ScriptMethodCall : IMethodCall
    {
        public ScriptMethodCall(string methodName, string methodNamespace, 
            string methodBody, IMethodCatalog catalog, params string[] parameterNames)
        {
            Name = methodName;
            Namespace = methodNamespace;
            _parameterNames = parameterNames;
            ScriptParser parser = new ScriptParser(catalog);
            body = parser.Parse(methodBody);
        }
        public string Name { get; set; }
        public string Namespace { get; set; }
        private string[] _parameterNames;
        public List<dynamic> ParameterValues { get; set; }
        IExecutionUnit body;
        public dynamic Execute(ExecutionScope scope)
        {
            return body.Execute(scope);
        }

        public string[] GetParameters()
        {
            return _parameterNames;
        }

        public string FullName
        {
            get { return string.Format("{0}.{1}", Name, Namespace); }
        }

    }
    
    public class ClrInstanceMethodCall : IMethodCall
    {
        public ClrInstanceMethodCall(string methodName, string methodNamespace, string clrType, string clrMethodname)
        {
            Name = methodName;
            Namespace = methodNamespace;
            ClrType = clrType;
            ClrMethodName = clrMethodname;
            Parameters = new List<dynamic>();
        }
        public string Name { get; set; }
        public string Namespace { get; set; }
        public string ClrType { get; set; }
        public string ClrMethodName { get; set; }
        public List<dynamic> Parameters { get; set; }

        public dynamic Execute(ExecutionScope scope)
        {
            var type = System.Type.GetType(this.ClrType);
            var method = type.GetMethod(this.ClrMethodName);
            object[] parameterValues = ApplyParameters(method, scope);
            var instance = Activator.CreateInstance(type);
            var result = method.Invoke(instance, parameterValues);
            return result;
        }

        private object[] ApplyParameters(System.Reflection.MethodInfo method, ExecutionScope scope)
        {
            List<object> parameterValues = new List<object>();
            foreach (var methodParameter in GetParameters())
            {
                var paramType = method.GetParameters().Where(x => x.Name == methodParameter).FirstOrDefault().ParameterType;
                object paramValue = scope.GetVariableValue(methodParameter);
                paramValue = ConversionUtil.Convert(paramValue, paramType);
                parameterValues.Add(paramValue);
            }
            return parameterValues.ToArray();
        }

        public string[] GetParameters()
        {
            var type = System.Type.GetType(this.ClrType);
            var method = type.GetMethod(this.ClrMethodName);
            return method.GetParameters().Select(x => x.Name).ToArray();
        }

        public string FullName
        {
            get { return string.Format("{0}.{1}", Name, Namespace); }
        }
    }

    public interface IParameter
    {
        string Name { get; set; }
        dynamic Value { get; set; }
    }

    public class DefaultParameter : IParameter
    {
        public string Name { get; set; }
        public dynamic Value { get; set; }
    }

    public class MathFunctions
    {
        [ClrInstanceMethodDeclaration(methodName:"add", methodNamespace:"framework.math")]
        public int Add(int num1, int num2)
        {
            return num1 + num2;
        }

        [ClrInstanceMethodDeclaration(methodName: "min", methodNamespace: "framework.math")]
        public int Min(int num1, int num2)
        {
            return Math.Min(num1,num2);
        }

        [ClrInstanceMethodDeclaration(methodName: "subtract", methodNamespace: "framework.math")]
        public int Subtract(int num1, int num2)
        {
            return num1 - num2;
        }
    }

    public class MethodCatalogBase : IMethodCatalog
    {
        public MethodCatalogBase()
        {
            _methods = new List<IMethodCall>();
        }
        public IMethodCall GetMethod(string methodNamespace, string methodName)
        {
            var methods = _methods.Where(x => x.Name == methodName &&
                x.Namespace == methodNamespace).ToArray();
            if (methods.Length > 1)
            {
                throw new ApplicationException("Multiple methods matched");
            }
            return methods.FirstOrDefault();
        }

        public IMethodCall GetMethod(string fullMethodName)
        {
            var indexToNamespaceSeparator = fullMethodName.LastIndexOf('.');
            string methodNamespace = fullMethodName.Substring(0, indexToNamespaceSeparator);
            string methodName = fullMethodName.Substring(indexToNamespaceSeparator + 1, fullMethodName.Length - (indexToNamespaceSeparator + 1));
            return GetMethod(methodNamespace, methodName);
        }

        protected List<IMethodCall> _methods;

        public void AddScriptMethod(string methodNameSpace, string methodName, string methodBody, params string[] parameterNames)
        {
            ScriptMethodCall methodCall = new ScriptMethodCall(methodName, methodNameSpace, methodBody, this, parameterNames);
            _methods.Add(methodCall);
        }

        public void AddMethod(ClrInstanceMethodCall call)
        {
            _methods.Add(call);
        }

        public void AddMethods(IEnumerable<IMethodCall> calls)
        {
            _methods.AddRange(calls);
        }
    }

    public class MockMethodCatalog : MethodCatalogBase
    {
        public MockMethodCatalog()
        {
            _methods = new List<IMethodCall>();
            LoadMethods();
        }

        private void LoadMethods()
        {
           AddMethod(new ClrInstanceMethodCall("add","framework","Innorovation.WorkflowSpike01.Console.MathFunctions","Add"));
            AddScriptMethod("add","custom","return framework.add(1, 2);");
        }
    }

    public class DefaultMethodCatalog : MethodCatalogBase
    {
        public DefaultMethodCatalog()
        {

        }
        public DefaultMethodCatalog(IEnumerable<IMethodCall> calls)
        {
            _methods.AddRange(calls);
        }
    }

    public class ScriptEngine
    {
        public ScriptEngine(IMethodCatalog catalog)
        {
            _catalog = catalog;
        }

        IMethodCatalog _catalog;

        public dynamic Run(string fullMethodName, Dictionary<string, object> variables)
        {
            if (_catalog == null) 
                throw new ApplicationException("Catalog does not exist, please esnure catalog is loaded");
            IMethodCall call = _catalog.GetMethod(fullMethodName);
            ExecutionScope scope = new ExecutionScope();
            if(variables!=null)
            {
                foreach(var variable in variables){
                    scope.SetVariableValue(variable.Key, variable.Value);
                }
            }
            var result = call.Execute(scope);
            return result;
        }
    }
    
    public class ScriptParser
    {
        public ScriptParser()
        {

        }

        public ScriptParser(IMethodCatalog catalog)
        {
            _catalog = catalog;
        }

        IMethodCatalog _catalog;

        public IExecutionUnit Parse(string text)
        {
            ScriptTokenStreamReader tokenReader = new ScriptTokenStreamReader(text);
            
            IExecutionUnit unit = ParseScriptBody(tokenReader);
            return unit;
        }
        
        Stack<IExecutionUnit> _unitStack = new Stack<IExecutionUnit>();

        private IExecutionUnit ParseScriptBody(ScriptTokenStreamReader tokenReader)
        {
            Token currentToken = null;
            List<IStatement> units=new List<IStatement>();
            while ((currentToken = tokenReader.Peek()) != null)
            {
                if (IsReturnKeyword(currentToken))
                {
                    IStatement returnUnit = ParseReturn(tokenReader);
                    units.Add(returnUnit);
                }
            }
            return new ScriptBody(units);
        }

        private IStatement ParseReturn(ScriptTokenStreamReader tokenReader)
        {
            ConsumeReturnKeyword(tokenReader);
            IExpression expressionUnit = ParseExpression(tokenReader);
            ConsumeSemiColonLiteral(tokenReader);
            return new ReturnStatement(expressionUnit);
        }

        private IExpression ParseExpression(ScriptTokenStreamReader tokenReader)
        {
            Token currentToken = null;
            Stack<IExpression> unitStack = new Stack<IExpression>();
            while ((currentToken = tokenReader.Peek()) != null)
            {
                if (IsSemicolon(currentToken) ||
                    IsTokenType(currentToken, TokenType.Comma) ||
                    IsTokenType(currentToken, TokenType.CloseParenthesis))
                {
                    //break without consuming
                    break;
                }
                else if (IsString(currentToken))
                {
                    var unit = new StringExpression(currentToken.Text);
                    Consume(tokenReader);
                    unitStack.Push(unit);
                }
                else if (IsNumeric(currentToken))
                {
                    var unit = new NumericExpression(currentToken.Text);
                    Consume(tokenReader);
                    unitStack.Push(unit);
                }
                else if (IsIdentifier(currentToken))
                {
                    var identifier = ParseMultipartIdentifier(tokenReader);
                    IExpression unit = null;
                    if (IsPeekedTokenType(tokenReader, TokenType.OpenParenthesis))
                    {
                        unit = ParseMethodExpression(identifier,tokenReader);
                    }
                    else
                    {
                        unit = new VariableExpression(identifier);
                    }
                    unitStack.Push(unit);
                }
                else if (IsBinaryExpressionToken(currentToken))
                {
                    IExpression lhs = unitStack.Pop();
                    Consume(tokenReader);
                    IExpression rhs = ParseExpressionPart(tokenReader);
                    var unit = GetBinaryExpressionFromToken(currentToken, lhs, rhs);
                    unitStack.Push(unit);
                }
                else
                    throw new ApplicationException("Invalid state");
            }
            if (unitStack.Count != 1) 
                throw new ApplicationException("Invalid state - stack had more than one element");
            return unitStack.Pop();
        }

        private bool IsTokenType(Token currentToken, TokenType expectedTokenType)
        {
            return currentToken.Type == expectedTokenType;
        }

        private bool IsPeekedTokenType(ScriptTokenStreamReader reader, TokenType expectedTokenType)
        {
            var currentToken = reader.Peek();
            if (currentToken == null) return false;
            return currentToken.Type == expectedTokenType;
        }

        private MethodCallExpression ParseMethodExpression(string identifier, ScriptTokenStreamReader tokenReader)
        {
            ConsumeNext(tokenReader, TokenType.OpenParenthesis);
            List<IExpression> methodParams = new List<IExpression>();
            bool commaContinuation = false;
            Token currentToken = null;
            while ((currentToken = tokenReader.Peek()) != null)
            {
                if (currentToken.Type == TokenType.CloseParenthesis)
                {
                    break;
                }
                IExpression expression = ParseExpression(tokenReader);
                if (commaContinuation && expression == null)
                    throw new ApplicationException("No expression after comma");
                methodParams.Add(expression);
                currentToken = tokenReader.Peek();
                if (currentToken.Type == TokenType.Comma)
                {
                    commaContinuation = true;
                    ConsumeNext(tokenReader, TokenType.Comma);
                }
                else
                {
                    commaContinuation = false;
                }
            }
            ConsumeNext(tokenReader, TokenType.CloseParenthesis);
            var methodCall = _catalog.GetMethod(identifier);
            var unit = new MethodCallExpression(methodCall,
                methodParams.ToArray());
            return unit;
        }

        private void ConsumeNext(ScriptTokenStreamReader tokenReader, TokenType tokenType)
        {
            var token = tokenReader.Read();
            if(token.Type !=  tokenType)
                throw new ApplicationException(string.Format("Expecting token of type {0} but encountered {1}", tokenType,
                    token.ToString()));
        }

        private string ParseMultipartIdentifier(ScriptTokenStreamReader tokenReader)
        {
            var currentToken = tokenReader.Peek();
            var identifierPart = currentToken.Text;
            Consume(tokenReader);
            while ((currentToken = tokenReader.Peek()) != null)
            {
                if (currentToken.Type != TokenType.Period) break;
                Consume(tokenReader);
                currentToken = tokenReader.Read();
                if (currentToken.Type != TokenType.Identifier)
                    throw new ApplicationException("Multipart identifier exception - identifier not found after period");
                identifierPart += "."+currentToken.Text;
            }
            return identifierPart;
        }

        private bool IsIdentifier(Token currentToken)
        {
            return currentToken.Type == TokenType.Identifier;
        }

        private IExpression ParseExpressionPart(ScriptTokenStreamReader tokenReader)
        {
            Token currentToken = null;
            IExpression unit=null;
            Stack<IExpression> unitStack = new Stack<IExpression>();
            while ((currentToken = tokenReader.Peek()) != null)
            {
                if (IsNumeric(currentToken))
                {
                    unit = new NumericExpression(currentToken.Text);
                    Consume(tokenReader);
                }
                else break;
            }
            return unit;
        }

        private Token Consume(ScriptTokenStreamReader tokenReader)
        {
            return tokenReader.Read();
        }

        private void ConsumeReturnKeyword(ScriptTokenStreamReader tokenReader)
        {
            Token returnKeyword = Consume(tokenReader);
            if (returnKeyword.Type != TokenType.Keyword && returnKeyword.Text != "return")
                throw new ApplicationException("return keyword not encountered");
        }

        private void ConsumeSemiColonLiteral(ScriptTokenStreamReader tokenReader)
        {
            Token semiColon = Consume(tokenReader);
            if (semiColon.Type != TokenType.Semicolon && semiColon.Text != ";") 
                throw new ApplicationException("semicolon not encountered");
        }

        private bool IsReturnKeyword(Token token)
        {
            return token.Type == TokenType.Keyword && token.Text == "return";
        }

        private bool IsString(Token token)
        {
            return token.Type == TokenType.String;
        }

        private bool IsNumeric(Token token)
        {
            return token.Type == TokenType.Numeric;
        }

        private bool IsSemicolon(Token token)
        {
            return token.Type == TokenType.Semicolon;
        }

        private bool IsAddition(Token token)
        {
            return token.Type == TokenType.PlusSign;
        }

        private bool IsSubtraction(Token token)
        {
            return token.Type == TokenType.Hyphen;
        }

        private bool IsMultiplication(Token token)
        {
            return token.Type == TokenType.Asterisk;
        }

        private bool IsDivision(Token token)
        {
            return token.Type == TokenType.ForwardSlash;
        }

        private bool IsModulo(Token token)
        {
            return token.Type == TokenType.Percent;
        }

        private bool IsBinaryExpressionToken(Token token)
        {
            return IsAddition(token) || IsSubtraction(token)
                || IsMultiplication(token) || IsDivision(token)
                || IsModulo(token);
        }

        private BinaryExpression GetBinaryExpressionFromToken(Token token, IExpression lhs, IExpression rhs)
        {
            if (IsAddition(token))
            {
                return new AdditionExpression(lhs, rhs);
            }
            else if (IsSubtraction(token))
            {
                return new SubtractionExpression(lhs, rhs);
            }
            else if (IsMultiplication(token))
            {
                return new MultiplicationExpression(lhs, rhs);
            }
            else if (IsDivision(token))
            {
                return new DivisionExpression(lhs, rhs);
            }
            else if (IsModulo(token))
            {
                return new ModuloExpression(lhs, rhs);
            }
            throw new ApplicationException(string.Format("Binary Expression not matched to token {0}", token.ToString()));
        }
    }

    public class ScriptTokenStreamReader
    {
        bool endOfStream;
        List<Token> bufferedTokens;
        ScriptTokenReader _underlyingReader;
        IEnumerable<Token> _tokenEnumerable;
        public ScriptTokenStreamReader(string text)
        {
            _underlyingReader = new ScriptTokenReader();
            _tokenEnumerable = _underlyingReader.Parse(text);
            bufferedTokens = new List<Token>();
        }

        public Token Peek()
        {
            if (IsEndOfFile()) return null;
            if (bufferedTokens.Count > 0)
            {
                return GetTokenFromBuffer(false);
            }
            else
            {
                ReadIntoBuffer(1);
                return GetTokenFromBuffer(false);
            }
        }

        public Token Read()
        {
            if (IsEndOfFile()) return null;
            if (bufferedTokens.Count > 0)
            {
                return GetTokenFromBuffer(true);
            }
            else
            {
                ReadIntoBuffer(1);
                return GetTokenFromBuffer(true);
            }
        }

        private Token GetTokenFromBuffer(bool removeAfterRead)
        {
            if (endOfStream && !removeAfterRead) return null;
            if (bufferedTokens.Count == 0) throw new ApplicationException("Buffer empty");
            var returnItem = bufferedTokens[0];
            if (removeAfterRead)
            {
                bufferedTokens.RemoveAt(0);
            }
            return returnItem;
        }

        private void ReadIntoBuffer(int numberToRead)
        {
            if (endOfStream) return;
            if (numberToRead == 0) return;
            int i = 0;
            foreach (var token in _tokenEnumerable)
            {
                bufferedTokens.Add(token);
                if (i >= numberToRead)
                {
                    return;
                }
                i++;
            }
            endOfStream = true;
        }

        private bool IsEndOfFile()
        {
            return endOfStream && bufferedTokens.Count == 0;
        }
    }
    
    public class ScriptTokenReader
    {
        public ScriptTokenReader()
        {
        }

        public IEnumerable<Token> Parse(string text)
        {
            TextReader reader = new StringReader(text);
            return Parse(reader);
        }

        private IEnumerable<Token> Parse(TextReader reader)
        {
            foreach (var item in ParseImpl(reader))
            {
                yield return item;
            }
        }

        private IEnumerable<Token> ParseImpl(TextReader reader)
        {
            int intValue = -1;
            while ((intValue=reader.Peek())!=-1)
            {
                var chr = (char)intValue;
                switch (chr)
                {
                    case ';':
                        Consume(reader);
                        yield return new Token(TokenType.Semicolon, ";");
                        break;
                    case '+':
                        Consume(reader);
                        yield return new Token(TokenType.PlusSign, "+");
                        break;
                    case '-':
                        Consume(reader);
                        yield return new Token(TokenType.Hyphen, "-");
                        break;
                    case '*':
                        Consume(reader);
                        yield return new Token(TokenType.Asterisk, "*");
                        break;
                    case '/':
                        Consume(reader);
                        yield return new Token(TokenType.ForwardSlash, "/");
                        break;
                    case '%':
                        Consume(reader);
                        yield return new Token(TokenType.Percent, "%");
                        break;
                    case '.':
                        Consume(reader);
                        yield return new Token(TokenType.Period, ".");
                        break;
                    case '(':
                        Consume(reader);
                        yield return new Token(TokenType.OpenParenthesis, "(");
                        break;
                    case ')':
                        Consume(reader);
                        yield return new Token(TokenType.CloseParenthesis, ")");
                        break;
                    case ',':
                        Consume(reader);
                        yield return new Token(TokenType.Comma, ",");
                        break;
                    case '\"':
                        Consume(reader);
                        var str = GetString(reader);
                        if (PeekCharIs(reader, '\"'))
                        {
                            Consume(reader);
                        }
                        else throw new ApplicationException("string incorrectly ended");
                        yield return new Token(TokenType.String, str);
                        break;
                    default:
                        if (char.IsLetter(chr))
                        {
                            string identifier = GetIdentifier(reader);
                            if (IsKeyword(identifier))
                            {
                                yield return new Token(TokenType.Keyword, identifier);
                            }
                            else
                            {
                                yield return new Token(TokenType.Identifier, identifier);
                            }
                        }
                        else if (char.IsDigit(chr))
                        {
                            var number = GetNumber(reader);
                            yield return new Token(TokenType.Numeric, number);
                        }
                        else if (chr == ' ')
                        {
                            Consume(reader);
                            continue;
                        }
                        else
                        {
                            throw new ApplicationException("Invalid state");
                        }
                        break;
                }
            }
        }

        private string GetNumber(TextReader reader)
        {
            List<char> identifierValues = new List<char>();
            int intValue = -1;
            bool floatingPoint = false;
            while ((intValue = reader.Peek()) != -1)
            {
                var chr = (char)intValue;
                if (char.IsDigit(chr))
                {
                    identifierValues.Add(chr);
                    Consume(reader);
                }
                else if (chr == '.')
                {
                    if (!floatingPoint)
                    {
                        identifierValues.Add(chr);
                        Consume(reader);
                    }
                    else throw new ApplicationException("multiple dot separators encountered for numeric type");
                }
                else break;
            }
            return new String(identifierValues.ToArray());
        }

        private bool IsKeyword(string identifier)
        {
            return _keywords.Contains(identifier);
        }

        private string GetIdentifier(TextReader reader)
        {
            List<char> identifierValues = new List<char>();
            int intValue = -1;
            //Ensure first part is letter
            if ((intValue = reader.Peek()) != -1)
            {
                var chr = (char)intValue;
                if(char.IsLetter(chr)){
                    identifierValues.Add(chr);
                    Consume(reader);
                }
                else{
                    throw new ApplicationException("Identifier must start with letter but does not");
                }
            }
            //subsequent parts can be letter or digit
            while ((intValue = reader.Peek()) != -1)
            {
                var chr = (char)intValue;
                if (char.IsLetterOrDigit(chr))
                {
                    identifierValues.Add(chr);
                    Consume(reader);
                }
                else break;
            }
            return new String(identifierValues.ToArray());
        }

        private string GetString(TextReader reader)
        {
            List<char> identifierValues = new List<char>();
            int intValue = -1;
            while ((intValue = reader.Peek()) != -1)
            {
                var chr = (char)intValue;
                if (PeekCharIs(reader, '\"'))
                {
                    break;
                }
                else if (chr == '\\')
                {
                    Consume(reader);
                    if (PeekCharIs(reader, '\"'))
                    {
                        identifierValues.Add(chr);
                        Consume(reader);
                    }
                }
                else
                {
                    identifierValues.Add(chr);
                    Consume(reader);
                }
            }
            return new String(identifierValues.ToArray());
        }

        private bool PeekCharIs(TextReader reader, char expectedChr)
        {
            var intValue = reader.Peek();
            if (intValue == -1) return false;
            char peekChr = (char)intValue;
            return peekChr == expectedChr;
        }

        private void Consume(TextReader reader)
        {
            reader.Read();
        }

        string[] _keywords = new string[]{
            "return",
            "function"
        };
    }

    public class Token
    {
        public Token(TokenType type, string text)
        {
            Text = text;
            Type = type;
        }
        public string Text { get; private set; }
        public TokenType Type { get; private set; }

        public override string ToString()
        {
            return string.Format("{0} - [{1}]", Text, Type);
        }
    }

    public enum TokenType
    {
        Semicolon,
        Keyword,
        Identifier,
        String,
        Numeric,
        PlusSign,
        Hyphen,
        Asterisk,
        ForwardSlash,
        Percent,
        Period,
        OpenParenthesis,
        CloseParenthesis,
        Comma
    }

    public class ConversionUtil
    {
        public static TOut Convert<TIn, TOut>(TIn input)
        {
            var output = System.Convert.ChangeType(input,typeof(TOut));
            return (TOut)output;
        }

        public static object Convert(object input, Type outputType)
        {
            var output = System.Convert.ChangeType(input, outputType);
            return output;
        }
    }

    public class WorkflowEngine
    {
        public WorkflowEngine(IMethodCatalog catalog)
        {
            _catalog = catalog;
        }

        IMethodCatalog _catalog;

        public WorkflowResponseMessage Handle(WorkflowRequestMessage request)
        {
            string resource = ResolveResource(request);
            WorkflowState state = ResolveState(request);
            WorkflowAction action = GetAction(request);

            var scriptToExecute = GetScriptToExecuteFrom(resource, state, action);

            var initialArguments = new Dictionary<string, object>();
            var engine =  new ScriptEngine(_catalog);
            var result = engine.Run(scriptToExecute, initialArguments);
            return result;
        }

        private string GetScriptToExecuteFrom(string resource, WorkflowState state, WorkflowAction action)
        {
            var configurationsForResource = configurations.Where(x => x.ResourceName == resource).ToList();
            if (configurationsForResource.Count > 1) 
                throw new ApplicationException(string.Format("Multiple configurations exist for resource", resource));
            var configurationForResource = configurationsForResource.FirstOrDefault();
            if (configurationForResource == null) 
                throw new ApplicationException(string.Format("No configuration found for resource", resource));

            var transitionMaps = configurationForResource.TransitionMaps.Where(x => x.ResourceName == resource
                && x.FromStateName == state.Name && x.RequestName == action.Name).ToList();

            var scriptsToRun = transitionMaps.Select(x => x.RequestScript).Distinct().ToList();
            if (scriptsToRun.Count != 1)
            {
                throw new ApplicationException(string.Format("Need exactly one matching script for resource {0}, action {1}, state {2}",
                    resource, action.Name, state.Name));
            }
            var scriptToRun = scriptsToRun.First();
            return scriptToRun;
        }

        private WorkflowAction GetAction(WorkflowRequestMessage request)
        {
            throw new NotImplementedException();
        }

        private WorkflowState ResolveState(WorkflowRequestMessage request)
        {
            throw new NotImplementedException();
        }

        private string ResolveResource(WorkflowRequestMessage request)
        {
            throw new NotImplementedException();
        }

        List<WorkflowConfiguration> configurations = new List<WorkflowConfiguration>();

    }

    public class WorkflowConfiguration
    {
        public string ResourceName { get; set; }
        public List<string> States { get; set; }
        public string InitialState { get; set; }
        public List<WorkflowTransitionMapEntry> TransitionMaps { get; set; }
    }

    public class WorkflowRequestMessage
    {
        public string Name { get; set; }
    }

    public class WorkflowAction
    {
        public string Name { get; set; }
    }

    public class WorkflowResponseMessage
    {
        public dynamic Model { get; set; }
        public List<string> Messages { get; set; }
    }

    public class WorkflowResponse
    {

    }

    public class WorkflowRequest
    {

    }

    public class WorkflowTransitionMapEntry
    {
        public string ResourceName { get; set; }
        public string FromStateName{get;set;}
        public string RequestName{get;set;}
        public string RequestResult{get;set;}
        public string ToStateName{get;set;}
        public string RequestValidation { get; set; }
        public string RequestScript{get;set;}
    }
    public class WorkflowState
    {
        public string Name { get; set; }
    }

    public class WorkflowBuilder
    {
        public WorkflowBuilder()
        {

        }

        private static Lazy<WorkflowBuilder> _factory = new Lazy<WorkflowBuilder>(() => new WorkflowBuilder());
        public static WorkflowBuilder Current
        {
            get
            {
                return _factory.Value;
            }
        }

        public IWorkflowAssemblyBuilder For(string resourceName)
        {
            WorkflowAssembly assembly = new WorkflowAssembly();
            assembly.For(resourceName);
            return assembly;
        }

        //public IWorkflowAssemblyBuilder WithStateResolver(string resolverTypeName)
        //{
        //    throw new NotImplementedException();
        //}

        //public IWorkflowAssemblyBuilder HasStates(params string[] states)
        //{
        //    throw new NotImplementedException();
        //}

        //public IWorkflowAssemblyBuilder WithInitialState(string state)
        //{
        //    throw new NotImplementedException();
        //}

        //public IWorkflowAssemblyBuilder WithTransitionMapEntry(string fromStateName, string actionName, string actionResult, string toStateName, string actionValidation, string actionScript)
        //{
        //    throw new NotImplementedException();
        //}
    }

    public class WorkflowAssembly : IWorkflowAssemblyBuilder
    {
        public WorkflowAssembly()
        {
            States = new List<string>();
            MapEntries = new List<WorkflowTransitionMapEntry>();
        }
        public string ResourceName { get; set; }
        public string StateResolverName { get; set; }
        public List<string> States { get; set; }
        public string InitialState { get; set; }
        public List<WorkflowTransitionMapEntry> MapEntries { get; set; }


        public IWorkflowAssemblyBuilder For(string resourceName)
        {
            this.ResourceName = resourceName;
            return this;
        }

        public IWorkflowAssemblyBuilder WithStateResolver(string resolverTypeName)
        {
            this.StateResolverName = resolverTypeName;
            return this;
        }

        public IWorkflowAssemblyBuilder HasStates(params string[] states)
        {
            this.States.AddRange(states);
            return this;
        }

        public IWorkflowAssemblyBuilder WithInitialState(string state)
        {
            this.InitialState = state;
            return this;
        }

        public IWorkflowAssemblyBuilder WithTransitionMapEntry(string fromStateName, string actionName, string actionResult, string toStateName, string actionValidation, string actionScript)
        {
            this.MapEntries.Add(new WorkflowTransitionMapEntry
            {
                ResourceName = this.ResourceName,
                FromStateName = fromStateName,
                RequestName = actionName,
                RequestResult = actionResult,
                RequestScript = actionScript,
                RequestValidation = actionValidation,
                ToStateName = toStateName
            });
            return this;
        }
    }

    public interface IWorkflowAssemblyBuilder
    {
        IWorkflowAssemblyBuilder For(string resourceName);
        IWorkflowAssemblyBuilder WithStateResolver(string resolverTypeName);
        IWorkflowAssemblyBuilder HasStates(params string[] states);
        IWorkflowAssemblyBuilder WithInitialState(string state);
        IWorkflowAssemblyBuilder WithTransitionMapEntry(string fromStateName, 
            string actionName, string actionResult, string toStateName,
            string actionValidation, string actionScript); 

    }

    public interface IWorkflowRepository
    {
        void SaveScripts(List<IMethodCall> methodCalls);
        void SaveScript(IMethodCall methodCall);
        void SaveWorkflow(WorkflowConfiguration workflow);
        List<IMethodCall> GetScripts();
    }

    public class DefaultWorkflowRepository:IWorkflowRepository
    {
        public DefaultWorkflowRepository()
        {
            _dataAccess = new RavenDataAccess("TestDb");
            _dataAccess.AddRavenCollectionOverrideForType(typeof(IMethodCall), "MethodCall");
            _dataAccess.AddRavenCollectionOverrideForType(typeof(ScriptMethodCall), "MethodCall");
            _dataAccess.AddRavenCollectionOverrideForType(typeof(ClrInstanceMethodCall), "MethodCall");

            _dataAccess.AddRavenIdOverrideForType(typeof(ScriptMethodCall), "FullName");
            _dataAccess.AddRavenIdOverrideForType(typeof(ClrInstanceMethodCall), "FullName");
        }

        RavenDataAccess _dataAccess;

        public void SaveScripts(List<IMethodCall> methodCalls)
        {
            foreach (var methodCall in methodCalls)
            {
                _dataAccess.Save<IMethodCall>(methodCall);
            }
        }

        public void SaveScript(IMethodCall methodCall)
        {
            _dataAccess.Save<IMethodCall>(methodCall);
        }

        public void SaveWorkflow(WorkflowConfiguration workflow)
        {
            throw new NotImplementedException();
        }
        
        public List<IMethodCall> GetScripts()
        {
            RavenQuery<IMethodCall> methodCallQuery = new RavenQuery<IMethodCall>();
            methodCallQuery.PageSize=50;
            methodCallQuery.ReadTillEnd = true;
            var results = _dataAccess.Query<IMethodCall>(methodCallQuery);
            return results.Results;
        }
    }


    public class ClrInstanceMethodDeclarationAttribute:Attribute
    {
        public ClrInstanceMethodDeclarationAttribute(string methodName, string methodNamespace)
        {
            MethodName = methodName;
            MethodNamespace = methodNamespace;
        }

        public string MethodName { get; private set; }
        public  string MethodNamespace { get; private set; }
    }
}