﻿using Microsoft.CSharp;
using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Razor;
using System.Web.WebPages;

namespace Innorovation.ControlCode.Razor
{
    public class ViewGenerator
    {
        public ViewGenerator()
            : this(null)
        { }

        public ViewGenerator(Dictionary<string, string> viewDefinitions)
        {
            Initialize();
            AddControls(viewDefinitions ?? new Dictionary<string, string>());
        }

        static Lazy<ViewGenerator> _instanceFactory = new Lazy<ViewGenerator>(() => new ViewGenerator());
        public static ViewGenerator Instance { get { return _instanceFactory.Value; } }

        Dictionary<string, string> _controlToSpecificationMap = new Dictionary<string, string>();
        Dictionary<string, string> _controlToTypeMap = new Dictionary<string, string>();
        const string controlNamespace = "Innorovation.Views";
        const string controlFormatString = "{0}Template";
        string _generatorAssemblyLocationPath;
        string _generatorNamespace;

        private static object _syncRoot = new object();
        Assembly _controlAssembly;

        private void Initialize()
        {
            //Get the assembly for this generator
            var generatorType = this.GetType();
            var assembly = generatorType.Assembly;
            var assemblyLocationPath = assembly.Location;
            //var assemblyLocationPath = string.Format("{0}{1}", AppDomain.CurrentDomain.BaseDirectory, assembly.ManifestModule.Name);

            var generatorNamespace = generatorType.Namespace;
            _generatorAssemblyLocationPath = assemblyLocationPath;
            _generatorNamespace = generatorNamespace;
        }

        public void AddControls(Dictionary<string, string> controlDefinitions)
        {
            foreach (var controlDefinition in controlDefinitions)
            {
                if (_controlToSpecificationMap.ContainsKey(controlDefinition.Key))
                {
                    _controlToSpecificationMap[controlDefinition.Key] = controlDefinition.Value;
                }
                else
                {
                    _controlToSpecificationMap.Add(controlDefinition.Key, controlDefinition.Value);
                }
            }
            if (_controlToSpecificationMap.Count > 0)
            {
                GenerateControlClasses();
            }
        }

        public void AddControl(string controlName, string controlMarkup)
        {
            AddControls(new Dictionary<string, string>
            {
                {controlName, controlMarkup}
            });
        }

        private void GenerateControlClasses()
        {
            List<CodeCompileUnit> compileUnits = new List<CodeCompileUnit>();
            var host = CreateHost();
            foreach (var entry in _controlToSpecificationMap)
            {
                var controlTypeName = string.Format(controlFormatString, entry.Key);
                var fullControlTypeName = string.Format("{0}.{1}", controlNamespace, controlTypeName);
                var codeUnit = GenerateViewCodeUnit(host, entry.Value, controlTypeName, controlNamespace);
                _controlToTypeMap.Add(entry.Key, fullControlTypeName);
                compileUnits.Add(codeUnit);
            }

            //var assembly = CompileToAssembly(string.Format("{0}RazorTemplateEngineTest.exe", AppDomain.CurrentDomain.BaseDirectory),
            //    compileUnits.ToArray());
            var assembly = CompileToAssembly(_generatorAssemblyLocationPath,
                compileUnits.ToArray());
            SetAssembly(assembly);
        }

        private void SetAssembly(Assembly assembly)
        {
            lock (_syncRoot)
            {
                _controlAssembly = assembly;
            }
        }

        private RazorEngineHost CreateHost()
        {
            var language = new CSharpRazorCodeLanguage();

            var host = new RazorEngineHost(language)
            {
                DefaultBaseClass = "CustomTemplateBase",
                DefaultClassName = "DemoTemplate",
                DefaultNamespace = "ProgrammingRazor"
            };
            return host;
        }

        private CodeCompileUnit GenerateViewCodeUnit(RazorEngineHost host, string text, string typeName, string typeNamespace)
        {
            var reader = new StringReader(text);

            try
            {
                // Create a new Razor Template Engine
                RazorTemplateEngine engine = new RazorTemplateEngine(host);
                engine.Host.NamespaceImports.Add("System");
                engine.Host.NamespaceImports.Add("System.Web.WebPages");
                engine.Host.NamespaceImports.Add(_generatorNamespace);

                // Generate code for the template
                GeneratorResults razorResult = engine.GenerateCode(reader, typeName, typeNamespace, null);
                var success = razorResult.Success;
                return razorResult.GeneratedCode;
            }
            catch (Exception exp)
            {
                //TODO need to swap this out
                Console.WriteLine(exp.ToString());
                return null;
            }
        }

        public string RenderView(string viewName, object model)
        {
            string namespaceToType = _controlToTypeMap[viewName];
            CustomTemplateBase template = null;

            //var template = (CustomTemplateBase)assembly.CreateInstance("ProgrammingRazor.DemoTemplate");
            lock (_syncRoot)
            {
                template = (CustomTemplateBase)_controlAssembly.CreateInstance(namespaceToType);
            }
            template.Model = model;
            template.Execute();
            return template.GetTemplateText();
        }

        private Assembly CompileToAssembly(string pathToDll, params CodeCompileUnit[] codeCompileUnits)
        {
            var compilerParameters = new CompilerParameters();
            compilerParameters.ReferencedAssemblies.AddRange(new string[] { 
                    "System.dll",
                    "System.Core.dll",
                    "System.Configuration.dll",
                    "Microsoft.CSharp.dll",
                    "System.Web.dll",
                    "System.Xml.dll",
                    "System.Xml.Linq.dll",
                    //"System.Web.WebPages.dll",
                    //"System.Web.Razor.dll",
                    //"System.Web.WebPages.Razor.dll",
                    string.Format("{0}\\bin\\System.Web.Razor.dll",AppDomain.CurrentDomain.BaseDirectory),
                    string.Format("{0}\\bin\\System.Web.WebPages.Razor.dll",AppDomain.CurrentDomain.BaseDirectory),
                    string.Format("{0}\\bin\\System.Web.WebPages.dll",AppDomain.CurrentDomain.BaseDirectory),
                    pathToDll
                });
            compilerParameters.GenerateInMemory = true;

            var provider = new CSharpCodeProvider();
            foreach (var unit in codeCompileUnits)
            {
                StringBuilder builder = new StringBuilder();
                using (var strwriter = new StringWriter(builder))
                {
                    provider.GenerateCodeFromCompileUnit(unit, strwriter, new CodeGeneratorOptions { });
                    var code = builder.ToString();
                }
            }

            CompilerResults compilerResults = new CSharpCodeProvider()
                                                .CompileAssemblyFromDom(
                                                compilerParameters,
                                                codeCompileUnits
                                                );

            var haserrors = compilerResults.Errors.HasErrors;
            if (haserrors)
            {
                foreach (var error in compilerResults.Errors)
                {
                    Console.WriteLine(error.ToString());
                }
            }
            var assembly = compilerResults.CompiledAssembly;
            //foreach (var type in assembly.GetTypes())
            //{
            //    Console.WriteLine(type.FullName);
            //}
            return assembly;
        }
    }

    public abstract class CustomTemplateBase : WebPageExecutingBase
    {
        public CustomTemplateBase()
        {
            sb = new StringBuilder();
            writer = new StringWriter(sb);
        }

        StringBuilder sb;
        TextWriter writer;
        private dynamic _model;
        public dynamic Model
        {
            get
            {
                return _model;
            }
            set { _model = value; }
        }

        public override void Write(HelperResult result)
        {
            var writer = GetOutputWriter();
            writer.Write(result.ToHtmlString());
        }

        public override void Write(object value)
        {
            var writer = GetOutputWriter();
            writer.Write(value);
        }
        public override void WriteLiteral(object value)
        {
            var writer = GetOutputWriter();
            writer.Write(value);
        }

        protected override TextWriter GetOutputWriter()
        {
            return writer;
        }

        public string GetTemplateText()
        {
            var writer = GetOutputWriter();
            if (writer != null)
            {
                writer.Flush();
                writer.Close();
            }
            return sb.ToString();
        }

        public string Control(string controlName, object model)
        {
            return ViewGenerator.Instance.RenderView(controlName, model);
        }

        public string Control(string controlName, Expression<Func<object, object>> propertyAccessor)
        {
            var scopedProperty = "";
            if (propertyAccessor.Body is MemberExpression)
            {
                scopedProperty = ((MemberExpression)propertyAccessor.Body).Member.Name;
            }
            var scopedModel = propertyAccessor.Compile().Invoke(Model);
            return Control(controlName, scopedModel);
        }
    }

    public interface ITypeInformationSource
    {
        TypeInformation GetTypeInformation();
    }

    public class TypeInformation
    {
        public List<PropertyInformation> Properties { get; set; }
    }

    public class PropertyInformation
    {
        public string PropertyName { get; set; }
        public Type PropertyType { get; set; }
        public string DisplayName { get; set; }
        public string IsMappedToMethod { get; set; }
        public bool IsCollection { get; set; }
        public bool IsPrimitive { get; set; }
        public string Format { get; set; }
        public TypeInformation TypeInfo { get; set; }
    }

    public static class ViewGeneratorExtensions
    {
        public static object GetProperty(this object item, string propertyName)
        {
            try
            {
                if (item == null) return null;
                if (item is ExpandoObject)
                {
                    return ((IDictionary<string, object>)item)[propertyName];
                }
                else
                {
                    var info = item.GetType().GetProperty(propertyName);
                    if (info == null) return null;
                    var scopeValue = info.GetValue(item);
                    return scopeValue;
                }
            }
            catch (Exception exp)
            {
                throw;
            }
        }
    }
}
